package com.example.account.project1;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void openPic1 (View view) {
        Intent intent = new Intent(this, Pic1Activity.class);
        startActivity(intent);
    }

    public void openPic2 (View view) {
        Intent intent = new Intent(this, Pic2Activity.class);
        startActivity(intent);
    }

    public void openPic3 (View view) {
        Intent intent = new Intent(this, Pic3Activity.class);
        startActivity(intent);
    }
}
